﻿using BugTrackingSystem.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTrackingSystem.BLL
{
   public class BLLEmployee
    {
       public DataTable GetMaxEmpCode()
       {

           DataTable dt = DAO.GetTable("select top 1 *from tblEmployee Order by EmpId Desc", null, CommandType.Text);
           return dt;

       }
       public int CreateEmployee(string empcode, string name, string email, string password, string mobile, string designation, DateTime joiningdate, string usertype, byte[] empimage, int projectid)
       {
           SqlParameter[] param = new SqlParameter[]
           {
               new SqlParameter("@a",empcode),
                new SqlParameter("@b",name),
                 new SqlParameter("@c",email),
                  new SqlParameter("@d",password),
                   new SqlParameter("@e",mobile),
                    new SqlParameter("@f",designation),
                     new SqlParameter("@g",joiningdate),
                      new SqlParameter("@h",usertype),
                       new SqlParameter("@i",empimage),
                        new SqlParameter("@j",projectid)
           };
           return DAO.IUD("insert into tblEmployee values(@a,@b,@c,@d,@e,@f,@g,@h,@i,@j)", param, CommandType.Text);
       }
        public int UpdateEmployee(string empcode, string name,  string mobile, string designation,  string usertype, byte[] empimage, int projectid, int empid)
        {
            SqlParameter[] param = new SqlParameter[]
            {
             
                new SqlParameter("@b",name),
                
                 
                   new SqlParameter("@e",mobile),
                    new SqlParameter("@f",designation),
                  
                      new SqlParameter("@h",usertype),
                       new SqlParameter("@i",empimage),
                        new SqlParameter("@j",projectid),
                            new SqlParameter("@k",empid)
            };
            return DAO.IUD("update tblEmployee set Name=@b,Mobile=@e,Designation=@f,Usertype=@h,EmpImage=@i,ProjectId=@j where EmpId=@k", param, CommandType.Text);
        }
        public int DeleteEmployee(int employeeid)
        {
            SqlParameter[] param = new SqlParameter[]
            {
               new SqlParameter("@a",employeeid)
               
            };
            return DAO.IUD("delete from tblEmployee where EmpId=@a", param, CommandType.Text);
        }
        public DataTable GetEmployeeNamebyProjectId(int projectid)
       {
           SqlParameter[] param = new SqlParameter[]
           {
               new SqlParameter("@a",projectid)
           };

           DataTable dt = DAO.GetTable("select *from tblEmployee where ProjectId=@a and Usertype='Developer'", param, CommandType.Text);
           return dt;

       }
        public DataTable GetEmployeeTesterNamebyProjectId(int projectid)
        {
            SqlParameter[] param = new SqlParameter[]
            {
               new SqlParameter("@a",projectid)
            };

            DataTable dt = DAO.GetTable("select *from tblEmployee where ProjectId=@a and Usertype='Tester'", param, CommandType.Text);
            return dt;

        }
        public DataTable CheckUserLogin(string email, string password, string usertype)
       {
           SqlParameter[] param = new SqlParameter[]
           {
              
                 new SqlParameter("@a",email),
                  new SqlParameter("@b",password),
                 
                      new SqlParameter("@c",usertype)
                     
           };
           DataTable dt = DAO.GetTable("select *from tblEmployee where Email=@a and Password=@b and Usertype=@c", param, CommandType.Text);
           return dt;
       }
        public DataTable GetEmployeeNameByEmpId(int empid)
        {
            SqlParameter[] param = new SqlParameter[]
            {
               new SqlParameter("@a",empid)
            };

            DataTable dt = DAO.GetTable("select *from tblEmployee where EmpId=@a", param, CommandType.Text);
            return dt;

        }
        public DataTable CheckUser(string email, string oldpassword)
        {
            SqlParameter[] param = new SqlParameter[]
            {

                 new SqlParameter("@a",email),
                  new SqlParameter("@b",oldpassword)

                     

            };
            DataTable dt = DAO.GetTable("select *from tblEmployee where Email=@a and Password=@b", param, CommandType.Text);
            return dt;
        }
        public int UpdatePassword(string email, string password)
        {
            SqlParameter[] param = new SqlParameter[]
            {
              
                 new SqlParameter("@c",email),
                  new SqlParameter("@d",password)
                 
            };
            return DAO.IUD("update tblEmployee set Password=@d where Email=@c", param, CommandType.Text);
        }
        public DataTable GetAllEmployee()
        {
           

            DataTable dt = DAO.GetTable("select *from tblEmployee", null, CommandType.Text);
            return dt;

        }
        public DataTable GetEmployeeNameByEmpName(string empname)
        {
            SqlParameter[] param = new SqlParameter[]
            {
               new SqlParameter("@a",empname+"%")
            };

            DataTable dt = DAO.GetTable("select *from tblEmployee where Name Like @a", param, CommandType.Text);
            return dt;

        }


    }
}

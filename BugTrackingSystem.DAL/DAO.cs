﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTrackingSystem.DAL
{
   public static class DAO
    {

       
       //connection garne method
       public static AppSettingsReader aps = new AppSettingsReader();
       public static SqlConnection GetConnection()
       {
           SqlConnection con = new SqlConnection(aps.GetValue("myconnection",typeof(string)).ToString());
           if (con.State != ConnectionState.Open)
           {
               con.Open();
           }
           return con;
       }
       public static int IUD(string sql, SqlParameter[] param, CommandType cmdType)
       {
           using (SqlConnection con =GetConnection())
           {
               using (SqlCommand cmd = new SqlCommand(sql, con))
               {
                   cmd.CommandType = cmdType;
                   if (param != null)
                   {
                       cmd.Parameters.AddRange(param);
                   }

                   
                   return cmd.ExecuteNonQuery();
               }
           }
       }

       //IUD ko lagi method banauchau
       //select ko lagi
       public static DataTable GetTable(string sql, SqlParameter[] param, CommandType cmdType)
       {
           using (SqlConnection con = GetConnection())
           {
               using (SqlCommand cmd = new SqlCommand(sql, con))
               {
                   cmd.CommandType = cmdType;
                   if (param != null)
                   {
                       cmd.Parameters.AddRange(param);
                   }
                   SqlDataAdapter da = new SqlDataAdapter(cmd);
                   DataTable dt = new DataTable();
                   da.Fill(dt);
                   return dt;

                   
               }
           }
       }

       //select ko method banauchau

    }
}

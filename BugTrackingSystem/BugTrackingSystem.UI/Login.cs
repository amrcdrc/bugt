﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BugTrackingSystem.BLL;

namespace BugTrackingSystem.UI
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        BLLEmployee blemp = new BLLEmployee();
        private void btnLogin_Click(object sender, EventArgs e)
        {
            DataTable dt = blemp.CheckUserLogin(txtUsername.Text, txtPassword.Text, cboUsertype.Text);
            if (dt.Rows.Count > 0)
            {
                Program.email = txtUsername.Text;
                Program.name = dt.Rows[0]["Name"].ToString();
                Program.empid = Convert.ToInt32(dt.Rows[0]["EmpId"].ToString());

                if (cboUsertype.Text == "Tester")
                {
                    CreateBug frm = new CreateBug();
                    frm.Show();
                    this.Hide();
                }
                else if (cboUsertype.Text == "Developer")
                {
                    DevelperView frm = new DevelperView();
                    frm.Show();
                    this.Hide();
                }


            }
            else
            {
                MessageBox.Show("Invalid User");

            }
            
        }
    }
}

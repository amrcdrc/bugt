﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BugTrackingSystem.BLL;

namespace BugTrackingSystem.UI
{
    public partial class DevelperView : Form
    {
        public DevelperView()
        {
            InitializeComponent();
        }

        BLLBug blb = new BLLBug();
        private void DevelperView_Load(object sender, EventArgs e)
        {
            DataTable dt = blb.GetAllAssignBugByEmployeeId(Program.empid, "Open");
            dataGridView1.DataSource = dt;
        }

        private void dataGridView1_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            ViewBugDetails frm = new ViewBugDetails();
            frm.ShowDialog();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BugTrackingSystem.DAL;
using System.Data;

namespace BugTrackingSystem.BLL
{
   public class BLLBug
    {
       public int CreateBug(int raisedempid, int assignedempid, int projectid, string bugtitle, string bugdescription, byte[] attachment, string bugstatus, string createddate, string fixeddate, string closedate)
       {
           SqlParameter[] param = new SqlParameter[]
           {
               new SqlParameter("@a",raisedempid),
                new SqlParameter("@b",assignedempid),
                 new SqlParameter("@c",projectid),
                  new SqlParameter("@d",bugtitle),
                   new SqlParameter("@e",bugdescription),
                    new SqlParameter("@f",attachment),
                     new SqlParameter("@g",bugstatus),
                      new SqlParameter("@h",createddate),
                       new SqlParameter("@i",fixeddate),
                        new SqlParameter("@j",closedate)
           };
           return DAO.IUD("insert into tblBug values(@a,@b,@c,@d,@e,@f,@g,@h,@i,@j)", param, CommandType.Text);
       }
       public DataTable GetAllAssignBugByEmployeeId(int assignempid, string bugstatus)
       {
           SqlParameter[] param = new SqlParameter[]
           {
               new SqlParameter("@a",assignempid),
                new SqlParameter("@b",bugstatus)
           };
           DataTable dt = DAO.GetTable("sp_GetAllAssignBugByempid", param, CommandType.StoredProcedure);
           return dt;
       }
    }
}

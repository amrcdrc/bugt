﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BugTrackingSystem.BLL;

namespace BugTrackingSystem.UI
{
    public partial class DeveloperSelect : MaterialSkin.Controls.MaterialForm
    {
        public DeveloperSelect()
        {
            InitializeComponent();
        }

        BLLProject blpro = new BLLProject(); //access business layer
        BLLEmployee blemp = new BLLEmployee();
        private void LoadProject()
        {
            DataTable dt = blpro.GetAllProject();

            DataRow dr = dt.NewRow();
            dr["Description"] = "Select Project";

            dt.Rows.InsertAt(dr, 0);

            cboProject.DataSource = dt;
            cboProject.DisplayMember = "Description";
            cboProject.ValueMember = "ProjectId";
        }

        private void cboProject_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboProject.SelectedIndex != 0)
            {
                DataTable dt = blemp.GetEmployeeNamebyProjectId(Convert.ToInt32(cboProject.SelectedValue.ToString()));
                cboDeveloper.DataSource = dt;
                cboDeveloper.DisplayMember = "Name";
                cboDeveloper.ValueMember = "EmpId";
            }
                
        }

        private void DeveloperSelect_Load(object sender, EventArgs e)
        {
            LoadProject();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Program.empid = Convert.ToInt32(cboDeveloper.SelectedValue.ToString());
            this.DialogResult = DialogResult.OK;
        }
    }
}

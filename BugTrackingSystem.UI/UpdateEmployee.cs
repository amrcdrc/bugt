﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BugTrackingSystem.BLL;
using System.IO;

namespace BugTrackingSystem.UI
{
    public partial class UpdateEmployee : MaterialSkin.Controls.MaterialForm
    {
        public UpdateEmployee()
        {
            InitializeComponent();
        }

        BLLProject blpro = new BLLProject();
        BLLEmployee blemp = new BLLEmployee();
       string browseimage = "";
       public int empid = 0;
       
        private void UpdateEmployee_Load(object sender, EventArgs e)
        {
            LoadProject();
            DataTable dt = blemp.GetEmployeeNameByEmpId(empid);
            txtEmpCode.Text = dt.Rows[0]["EmpCode"].ToString();
            txtName.Text = dt.Rows[0]["Name"].ToString();
            txtMobile.Text = dt.Rows[0]["Mobile"].ToString();
            cboDesignation.Text = dt.Rows[0]["Designation"].ToString();
            cboUsertype.Text = dt.Rows[0]["Usertype"].ToString();
           Program.empimage = (byte[])dt.Rows[0]["EmpImage"];
            ddlProject.SelectedValue = dt.Rows[0]["ProjectId"];

            Image newImage;
            using (MemoryStream ms = new MemoryStream(Program.empimage, 0, Program.empimage.Length))
            {
                ms.Write(Program.empimage, 0, Program.empimage.Length);

                newImage = Image.FromStream(ms, true);
            }
            pictureBox1.Image = newImage;
        }
        private void LoadProject()
        {
            DataTable dt = blpro.GetAllProject();

            DataRow dr = dt.NewRow();
            dr["Description"] = "Select Project";

            dt.Rows.InsertAt(dr, 0);

            ddlProject.DataSource = dt;
            ddlProject.DisplayMember = "Description";
            ddlProject.ValueMember = "ProjectId";
        }

        private void btnAddNewEmp_Click(object sender, EventArgs e)
        {
            if (browseimage.Length > 0)
            {
                Program.empimage = Helper.ReadFile(browseimage);
            }
            int i = blemp.UpdateEmployee(txtEmpCode.Text, txtName.Text, txtMobile.Text, cboDesignation.Text, cboUsertype.Text, Program.empimage, Convert.ToInt32(ddlProject.SelectedValue.ToString()), empid);
            if (i > 0)
            {
                MessageBox.Show("Employee Updated");
                this.DialogResult = DialogResult.OK;
            }

        }

       
             private void btnBrowse_Click(object sender, EventArgs e)
        {
            //only jpg format images
            openFileDialog1.Filter = "All jpg image (*.jpg)|*.jpg";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                browseimage = openFileDialog1.FileName;
                pictureBox1.ImageLocation = browseimage;
            }
        
    }
    }
}

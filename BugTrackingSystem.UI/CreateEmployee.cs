﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BugTrackingSystem.BLL;

namespace BugTrackingSystem.UI
{
    public partial class CreateEmployee : MaterialSkin.Controls.MaterialForm
    {
        public CreateEmployee()
        {
            InitializeComponent();
        }

        public UpdateEmployee UpdateEmployee
        {
            get => default(UpdateEmployee);
            set
            {
            }
        }

        BLLProject blpro = new BLLProject();
        BLLEmployee blemp = new BLLEmployee();
        string defaultimage = "";
        string browseimage = "";
        private void CreateEmployee_Load(object sender, EventArgs e)
        {
            cboUsertype.SelectedIndex = 0;
            cboDesignation.SelectedIndex = 0;
            //set noimage
            defaultimage = Application.StartupPath + "//noimage.jpg";
            pictureBox1.ImageLocation = defaultimage;

            LoadProject();

            LoadMaxEmpCode();

        }

        private void LoadMaxEmpCode()
        {
            DataTable dt = blemp.GetMaxEmpCode();
            if (dt.Rows.Count > 0)
            {
                string empcode = dt.Rows[0]["EmpCode"].ToString();
                string[] strarr = empcode.Split('-');
                int maxno = Convert.ToInt32(strarr[1].ToString());
                txtEmpCode.Text = "EMP-" + (maxno + 1).ToString();
            }
            else
            {
                txtEmpCode.Text = "EMP-1";
            }
        }

        private void LoadProject()
        {
            DataTable dt = blpro.GetAllProject();

            DataRow dr = dt.NewRow();
            dr["Description"] = "Select Project";

            dt.Rows.InsertAt(dr, 0);

            ddlProject.DataSource = dt;
            ddlProject.DisplayMember = "Description";
            ddlProject.ValueMember = "ProjectId";
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            //only jpg format images
            openFileDialog1.Filter = "All jpg image (*.jpg)|*.jpg";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                browseimage = openFileDialog1.FileName;
                pictureBox1.ImageLocation = browseimage;
            }
        }

        private void btnAddNewEmp_Click(object sender, EventArgs e)
        {
            if (cboUsertype.SelectedIndex == 0 || txtName.Text.Length == 0 || txtEmail.Text.Length == 0 || ddlProject.SelectedIndex == 0 || txtMobile.Text.Length==0|| txtPassword.Text.Length==0)
            {
                MessageBox.Show("All Fields Required");

            }
            else {
                if (txtPassword.Text == txtConfirmPassword.Text)
                {
                    byte[] imgbyte = null;
                    if (browseimage != "")
                    {

                        imgbyte = Helper.ReadFile(browseimage);
                    }
                    else
                    {
                        imgbyte = Helper.ReadFile(defaultimage);
                    }
                    int i = blemp.CreateEmployee(txtEmpCode.Text, txtName.Text, txtEmail.Text, txtPassword.Text, txtMobile.Text, cboDesignation.Text, DateTime.Today, cboUsertype.Text, imgbyte, Convert.ToInt32(ddlProject.SelectedValue.ToString()));
                    if (i > 0)
                    {
                        LoadMaxEmpCode();
                        MessageBox.Show("EmployeeCreated");
                    }
                }
                else
                {
                    MessageBox.Show("Password Mismatch");
                    txtPassword.Text = "";
                    txtConfirmPassword.Text = "";
                    txtPassword.Focus();
                }
            }
           
        }

      
        private void btnViewEmployee_Click(object sender, EventArgs e)
        {
            ViewEmployee frm = new ViewEmployee();
            frm.Show();
            this.Hide();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
    }
}

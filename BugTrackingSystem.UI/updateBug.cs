﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugTrackingSystem.UI
{
    public partial class updateBug : MaterialSkin.Controls.MaterialForm
    {
        String id;
        public updateBug(String e) // pass string e as string id 
        {
            id = e;
            InitializeComponent();


            SqlConnectionStringBuilder csb = new SqlConnectionStringBuilder();
            csb.DataSource = "AMOGH"; // database connection server name
            csb.InitialCatalog = "BugTrackingSystemDB";
            csb.IntegratedSecurity = true;

            string raisedEmpId, BugTitle, BugDesc, AssingempId, ProjectId, BugStatus;

            string connString = csb.ToString();
            string queryString = "select * from tblBug where BugId = " + id;
            txtBugId.Text = id;


            string queryStringRaisedEmp, queryStringProject, queryStringAssignedEmp;

            using (SqlConnection connection = new SqlConnection(connString))
            using (SqlCommand command = connection.CreateCommand())
            {
                command.CommandText = queryString;
                connection.Open();

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        raisedEmpId = reader["raisedEmpId"].ToString();
                        AssingempId = reader["AssingEmpId"].ToString();
                        ProjectId = reader["ProjectId"].ToString();
                        BugTitle = reader["Bugtitle"].ToString();
                        BugDesc = reader["BugDescription"].ToString();
                        BugStatus = reader["BugStatus"].ToString();

                        cboBugStatus.Text = BugStatus;
                        txtBugTitle.Text = BugTitle;
                        txtBugCode.Text = BugDesc;
                        txtBugRaised.Text = raisedEmpId;
                        cboProjectTitle.Text = ProjectId;
                        txtEmployee.Text = AssingempId;

                    }

                }
            }

            queryStringRaisedEmp = "select * from tblEmployee where EmpId = " + txtBugRaised.Text;
            queryStringProject = "select * from tblProject where ProjectId = " + cboProjectTitle.Text;
            queryStringAssignedEmp = "select * from tblEmployee where EmpId = " + txtEmployee.Text;

            using (SqlConnection connection = new SqlConnection(connString))
            using (SqlCommand command = connection.CreateCommand())
            {
                command.CommandText = queryStringRaisedEmp;
                connection.Open();

                using (SqlDataReader reader = command.ExecuteReader())
                {
                      while (reader.Read())
                        {
                           txtBugRaised.Text = reader["Name"].ToString();
                        }
                }
            }

            using (SqlConnection connection = new SqlConnection(connString))
            using (SqlCommand command = connection.CreateCommand())
            {
                command.CommandText = queryStringProject;
                connection.Open();

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        cboProjectTitle.Text = reader["Description"].ToString();
                    }
                }
            }

            using (SqlConnection connection = new SqlConnection(connString))
            using (SqlCommand command = connection.CreateCommand())
            {
                command.CommandText = queryStringAssignedEmp;
                connection.Open();

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        txtEmployee.Text = reader["Name"].ToString(); 
                    }
                }
            }
            
        }
        
       
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            string bgsts = cboBugStatus.Text;
            string codePart = txtBugCode.Text;
            int bgId = Convert.ToInt32( txtBugId.Text);

            string conString = "Data Source=AMOGH;Initial Catalog=BugTrackingSystemDB;Integrated Security=True"; // this is connection string
           
            SqlConnection sqlCon = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand();
            cmd = sqlCon.CreateCommand();
            cmd.CommandText = @"UPDATE tblBug SET BugStatus = '" + bgsts +"', BugDescription = '" + codePart + "' WHERE bugId = '" + bgId + "';";

            sqlCon.Open();
            cmd.ExecuteNonQuery();
            sqlCon.Close();
            
            MessageBox.Show("Data has been updated Successfully");
            this.DialogResult = DialogResult.OK;


            Console.WriteLine(bgsts + codePart + "\n" + txtBugId.Text);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}

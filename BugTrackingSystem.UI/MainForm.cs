﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugTrackingSystem.UI
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        public CreateEmployee CreateEmployee
        {
            get => default(CreateEmployee);
            set
            {
            }
        }

        public ChangePassword ChangePassword
        {
            get => default(ChangePassword); // same changepw
            set
            {
            }
        }

        public ViewEmployee ViewEmployee
        {
            get => default(ViewEmployee); // same viewemp
            set
            {
            }
        }

        public TesterView TesterView
        {
            get => default(TesterView); // same tester view
            set
            {
            }
        }

        public DevelperView DevelperView
        {
            get => default(DevelperView); //same developerview
            set
            {
            }
        }

        public TesterSelect TesterSelect
        {
            get => default(TesterSelect); //same tester select
            set
            {
            }
        }

        public DeveloperSelect DeveloperSelect
        {
            get => default(DeveloperSelect); // same developer select
            set
            {
            }
        }

        private void logoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Login frm = new UI.Login(); // new object create
            frm.Show(); //show login
            this.Hide(); //hide current

        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit(); //exit app
        }

        private void changePasswordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ChangePassword frm = new UI.ChangePassword(); // new obj
            frm.MdiParent = this; //assign parent  form
            frm.Show(); //changepassword show
        }

        private void createEmployeeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CreateEmployee frm = new UI.CreateEmployee();
            frm.MdiParent = this;
            frm.Show(); //similar
        }

        private void viewBugStatusToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DevelperView frm = new UI.DevelperView();
            frm.MdiParent = this;
            frm.Show(); //similar
        }

        private void assignBugToolStripMenuItem_Click(object sender, EventArgs e)
        {

            TesterView frm = new UI.TesterView();
            frm.MdiParent = this;
            frm.Show(); //similar
        }

        private void notepadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("notepad.exe"); //run notepad command
        }

        private void calculatorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("calc.exe"); //run calculator command
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            CreateEmployee frm = new UI.CreateEmployee();
            frm.MdiParent = this;
            frm.Show(); //same
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            CreateBug frm = new UI.CreateBug();
            frm.MdiParent = this;
            frm.Show(); //same
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            if (Program.usertype == "Tester") // check if deeloper tester or admin and show thier respective forms
            {
                TesterView frm = new UI.TesterView();
                frm.MdiParent = this;
                frm.Show();
            }
            else if (Program.usertype == "Developer")
            {
                DevelperView frm = new UI.DevelperView();
                frm.MdiParent = this;
                frm.Show();
            }
            else if (Program.usertype == "Admin")
            {
                DevelperView frm = new UI.DevelperView();
                frm.MdiParent = this;
                frm.Show();
            }
        }


        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            Login frm = new UI.Login();
            frm.Show();
            this.Hide(); //same
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            lblFullname.Text ="Welcome:  "+ Program.name; //status bar text.
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lblTime.Text = DateTime.Now.ToLongTimeString(); //statusbar
        }

        private void statusStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void editEmployeeToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}

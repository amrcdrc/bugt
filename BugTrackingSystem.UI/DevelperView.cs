﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BugTrackingSystem.BLL;

namespace BugTrackingSystem.UI
{
    public partial class DevelperView : MaterialSkin.Controls.MaterialForm
    {
        public DevelperView()
        {
            InitializeComponent();
        }

        BLLBug blb = new BLLBug();
        private void DevelperView_Load(object sender, EventArgs e)
        {
            if(Program.usertype=="Admin")
            {

                DeveloperSelect frm = new UI.DeveloperSelect();

                frm.ShowDialog();
            }
            LoadOpenBug();
            LoadFixedBug();
            LoadClosedBug();
        }

        private void LoadClosedBug()
        {
            DataTable dt = blb.GetAllAssignBugByEmployeeIdClosed(Program.empid, "Closed");
            dataGridView3.DataSource = dt;
           
        }

        private void LoadFixedBug()
        {
            DataTable dt = blb.GetAllAssignBugByEmployeeIdFixed(Program.empid, "Fixed");
            dataGridView2.DataSource = dt;
        }

        private void LoadOpenBug()
        {
            DataTable dt = blb.GetAllAssignBugByEmployeeId(Program.empid, "Open");
            dataGridView1.DataSource = dt;
        }

        private void dataGridView1_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            ViewBugDetails frm = new ViewBugDetails();
            frm.bugid = Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value.ToString());
            if (frm.ShowDialog() == DialogResult.OK)
            {
                LoadOpenBug();
                LoadFixedBug();
                LoadClosedBug();

            }
        }

        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            CreateBug c = new CreateBug();
            c.Show();
        }

        private void dataGridView3_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            CreateBug c = new CreateBug();
            c.Show();
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }
    }
}

﻿using BugTrackingSystem.BLL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugTrackingSystem.UI
{
    public partial class TesterView : MaterialSkin.Controls.MaterialForm
    {
        public TesterView()
        {
            InitializeComponent();
        }

        public CreateBug CreateBug
        {
            get => default(CreateBug);
            set
            {
            }
        }

        public updateBug updateBug
        {
            get => default(updateBug);
            set
            {
            }
        }

        BLLBug blb = new BLLBug();
        private void TesterView_Load(object sender, EventArgs e)
        {
            if (Program.usertype == "Admin")
            {

                TesterSelect frm = new UI.TesterSelect();

                frm.ShowDialog();
            }
            LoadOpenBug();
            LoadFixedBug();
            LoadClosedBug();
        }
        private void LoadClosedBug()
        {
            DataTable dt = blb.GetAllRaisedBugByEmployeeId(Program.empid, "Closed");
            dataGridView3.DataSource = dt;
        }

        private void LoadFixedBug()
        {
            DataTable dt = blb.GetAllRaisedBugByEmployeeIdFixed(Program.empid, "Fixed");
            dataGridView2.DataSource = dt;
        }

        private void LoadOpenBug()
        {
            DataTable dt = blb.GetAllRaisedBugByEmployeeId(Program.empid, "Open");
            dataGridView1.DataSource = dt;
        }

       /* private void button1_Click(object sender, EventArgs e)
        {
            CreateBug frm = new UI.CreateBug();
            if (frm.ShowDialog() == DialogResult.OK)
            {
                LoadOpenBug();
                LoadFixedBug();
                LoadClosedBug();
            }
        }*/

        private void button2_Click(object sender, EventArgs e)
        {
            String selectedValue = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
            // Console.WriteLine("The acquired id is " + selectedValue);

            updateBug form = new updateBug(selectedValue);
            if (form.ShowDialog() == DialogResult.OK)
            {
                LoadOpenBug();
                LoadFixedBug();
                LoadClosedBug();
            }

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BugTrackingSystem.BLL;
using ICSharpCode.TextEditor.Document;
using System.IO;

namespace BugTrackingSystem.UI
{
    public partial class CreateBug : MaterialSkin.Controls.MaterialForm
    {
        public CreateBug()
        {
            InitializeComponent();
        }

        BLLProject blpro = new BLLProject(); // access business layer
        BLLEmployee blemp = new BLLEmployee();
        BLLBug blbug = new BLLBug();
        string defaultimage = "";
        string browseimage = "";
        private void CreateBug_Load(object sender, EventArgs e)
        {
            LoadProject();
            cboBugStatus.SelectedIndex = 0;
        }
        private void LoadProject()
        {
            DataTable dt = blpro.GetAllProject();

            DataRow dr = dt.NewRow();
            dr["Description"] = "Select Project";

            dt.Rows.InsertAt(dr, 0);

            cboProject.DataSource = dt;
            cboProject.DisplayMember = "Description";
            cboProject.ValueMember = "ProjectId";
        }

        private void cboProject_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboProject.SelectedIndex != 0)
            {
                DataTable dt = blemp.GetEmployeeNamebyProjectId(Convert.ToInt32(cboProject.SelectedValue.ToString()));
                cboBugAssignedTo.DataSource = dt;
                cboBugAssignedTo.DisplayMember = "Name";
                cboBugAssignedTo.ValueMember = "EmpId";
            }
        }

        private void btnCreateBug_Click(object sender, EventArgs e)
        {
            if (txtBugTitle.Text.Length == 0 || txtBugDescripiton.Text.Length == 0 || cboProject.SelectedIndex == 0 || txtAttachment.Text.Length== 0)
            {
                MessageBox.Show("All Fields Required");
            }
            else
            {

          
            byte[] attachment = null;

            if (browseimage == "")
            {
                attachment = Helper.ReadFile(Application.StartupPath + "//noimage.jpg");

            }
            else
            {
                attachment = Helper.ReadFile(browseimage);
            }

            int i = blbug.CreateBug(Program.empid, Convert.ToInt32(cboBugAssignedTo.SelectedValue.ToString()), Convert.ToInt32(cboProject.SelectedValue.ToString()), txtBugTitle.Text, txtBugDescripiton.Text, attachment, cboBugStatus.Text, DateTime.Today.ToShortDateString(), "", "");
            if (i > 0)
            {
                MessageBox.Show("Bug Assigned To Developer Successfully");
                this.DialogResult = DialogResult.OK;
            }
            }


        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            //only jpg format images
            openFileDialog1.Filter = "All jpg image (*.jpg)|*.jpg";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                browseimage = openFileDialog1.FileName;
                txtAttachment.Text = browseimage;

            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtBugDescripiton_Load(object sender, EventArgs e)
        {
            string dirc = Application.StartupPath;
            FileSyntaxModeProvider fsmp;
            if (Directory.Exists(dirc))
            {

                fsmp = new FileSyntaxModeProvider(dirc);
                HighlightingManager.Manager.AddSyntaxModeFileProvider(fsmp);
                txtBugDescripiton.SetHighlighting("C#");

            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BugTrackingSystem.BLL;

namespace BugTrackingSystem.UI
{
    public partial class ChangePassword : MaterialSkin.Controls.MaterialForm
    {
        public ChangePassword()
        {
            InitializeComponent();
        }

        BLLEmployee blemp = new BLLEmployee();
        private void button1_Click(object sender, EventArgs e)
        {
           // if (txtUserName.Text== )
            //{
                
           // }
            if (txtOldPassword.Text == "" && txtNewPassword.Text=="") // check if pw empty
            {
                MessageBox.Show("All Fields Required");
            }
            else
            {
                if (txtNewPassword.Text == txtConfirmNew.Text) // check if pw match
                {
                    DataTable dt = blemp.CheckUser(Program.email, txtOldPassword.Text); // from business layer checkuser
                    if (dt.Rows.Count > 0)
                    {
                        int i = blemp.UpdatePassword(Program.email, txtNewPassword.Text); // from bl update pw
                        if (i > 0)
                        {
                            MessageBox.Show("Password Changed Successfully");

                        }
                    }
                    else
                    {
                        MessageBox.Show("User Doesnot Exists");
                    }

                }
                else
                {
                    MessageBox.Show("Password Mismatch");
                    txtConfirmNew.Text = "";
                    txtNewPassword.Text = "";
                    txtNewPassword.Focus();
                }
            
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ChangePassword_Load(object sender, EventArgs e)
        {

        }
    }
}

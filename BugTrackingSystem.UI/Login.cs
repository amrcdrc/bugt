﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BugTrackingSystem.BLL;

namespace BugTrackingSystem.UI
{
    public partial class Login : MaterialSkin.Controls.MaterialForm
    {
        public Login()
        {
            InitializeComponent();
        }

        public MainForm MainForm
        {
            get => default(MainForm); // get to default as mainform
            set
            {
            }
        }

        BLLEmployee blemp = new BLLEmployee(); // 3tier add business logic for data access
        private void btnLogin_Click(object sender, EventArgs e) // login button click event
        {
            if (String.IsNullOrEmpty(txtUsername.Text) || String.IsNullOrEmpty(txtPassword.Text) || cboUsertype.SelectedIndex==0)
            {
                MessageBox.Show("Please Enter Username or Password or Usertype", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtUsername.Focus();
                // check username pw null or empty or if not selected role
            }
            else // else connect
            {

                DataTable dt = blemp.CheckUserLogin(txtUsername.Text, txtPassword.Text, cboUsertype.Text);
                if (dt.Rows.Count > 0) // if data gets filled dt.rows.count will be greater than 0
                {
                    Program.email = txtUsername.Text;
                    Program.name = dt.Rows[0]["Name"].ToString();
                    Program.empid = Convert.ToInt32(dt.Rows[0]["EmpId"].ToString());
                    Program.usertype = cboUsertype.Text;
                    MainForm frm = new UI.MainForm();
                    if (cboUsertype.Text == "Tester")
                    {
                        frm.adminToolStripMenuItem.Enabled = false;
                        frm.developerToolStripMenuItem.Enabled = false;
                        frm.toolStripButton1.Enabled = false;
                        frm.toolStripButton2.Enabled = true;
                        frm.Show();
                        this.Hide(); // self explanatory
                    }
                    else if (cboUsertype.Text == "Developer")
                    {
                        frm.adminToolStripMenuItem.Enabled = false;
                        frm.testerToolStripMenuItem.Enabled = false;
                        frm.toolStripButton1.Enabled = false;
                        frm.toolStripButton2.Enabled = false;
                        frm.Show();

                        this.Hide();// self explanatory
                    }
                    else if (cboUsertype.Text == "Admin")
                    {
                        frm.toolStripButton2.Enabled = false;
                        frm.Show();

                        this.Hide();// self explanatory
                    }


                }
                else
                {
                    MessageBox.Show("Invalid User"); // if none invalid user.

                }
            }
            
        }

        private void Login_Load(object sender, EventArgs e)
        {
            cboUsertype.SelectedIndex = 0;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Application.Exit(); //exits application
        }

        private void txtUsername_TextChanged(object sender, EventArgs e)
        {

        }
    }
}

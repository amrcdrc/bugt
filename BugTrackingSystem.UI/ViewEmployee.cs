﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BugTrackingSystem.BLL;

namespace BugTrackingSystem.UI
{
    public partial class ViewEmployee : MaterialSkin.Controls.MaterialForm
    {
        public ViewEmployee()
        {
            InitializeComponent();
        }

        BLLEmployee blemp = new BLLEmployee();
        private void ViewEmployee_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = blemp.GetAllEmployee();


        }

        private void txtEmpname_TextChanged(object sender, EventArgs e)
        {
            DataTable dt = blemp.GetEmployeeNameByEmpName(txtEmpname.Text);
            dataGridView1.DataSource = dt;
        }

        public int empid = 0;
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                empid = Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value.ToString());
                UpdateEmployee frm = new UI.UpdateEmployee();
                frm.empid = empid;
                if(frm.ShowDialog()==DialogResult.OK)
                {
                    dataGridView1.DataSource = blemp.GetAllEmployee();
                }
            }
            else
            {
                MessageBox.Show("Please Select Record to Edit");
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure want to delete?", "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {
                if (dataGridView1.SelectedRows.Count > 0)
                {
                    empid = Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value.ToString());
                    int i = blemp.DeleteEmployee(empid);
                    if (i > 0)
                    {
                        dataGridView1.DataSource = blemp.GetAllEmployee();
                        MessageBox.Show("Employee Deleted Successfully");

                    }
                }
            }
        }
    }
}
